const nodeExternals = require('webpack-node-externals')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

const path = require('path');

export default {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Jemersoft - Dousdebes',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Test' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
    ]
  },
  plugins: ['~/plugins/vuetify.js'],
  css: ['~assets/style/tailwind.css'],

  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },

  buildModules: ['@nuxtjs/axios'],

  /*
  ** Build configuration
  */
  build: {

    /**
     * Tailwindcss configuration.
     */
    postcss: {
      plugins: {
        tailwindcss: path.resolve(__dirname, './assets/style/tailwind.config.js'),
      },
    },

    transpile: [/^vuetify/],
    plugins: [
      new VuetifyLoaderPlugin()
    ],
    hotMiddleware: {
      client: {
        overlay: false
      }
    },

    extractCSS: true,
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (process.server) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/]
          })
        ]
      }
    }
  }
}
